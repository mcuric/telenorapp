package com.example.telenor.service;

import org.springframework.stereotype.Service;

@Service
public interface TelenorService {
    String handle(Integer id, String account, String type);
}
