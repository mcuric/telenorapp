package com.example.telenor.service;

import com.example.telenor.exception.NotAllowableException;
import com.example.telenor.validator.DataValidator;
import org.springframework.stereotype.Service;

@Service
public class TelenorServiceImpl implements TelenorService {

    public static final String WELCOME_BUSINESS_USER = "Welcome, business user!";
    public static final String HI_USER_ID = "Hi, userId ";

    @Override
    public String handle(Integer id, String account, String type) {

        validateInput(id, account, type);

        if (isBigBusinessUser(account, type)) {
            return WELCOME_BUSINESS_USER;
        }

        return HI_USER_ID.concat(id.toString());
    }

    public void validateInput(Integer id, String account, String type) {

        if (!DataValidator.validateAccount(account)) {
            throw new NotAllowableException("account", account, "allowable values for account are personal and business");
        }

        if (!DataValidator.isNumberPositive(id)) {
            throw new NotAllowableException("id", id.toString(), "allowable values for id are a positive number");
        }

        if (!DataValidator.validateType(type)) {
            throw new NotAllowableException("type", type, " the allowable values for type are small and big");
        }

        if (isSmallBusinessUser(account, type)) {
            throw new NotAllowableException("type", type, " That the path is not yet implemented!");
        }

    }

    public boolean isBigBusinessUser(String account, String type) {
        return account.equals("business") && type.equals("big");
    }

    public boolean isSmallBusinessUser(String account, String type) {
        return account.equals("business") && type.equals("small");
    }

}
