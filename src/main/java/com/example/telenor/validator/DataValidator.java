package com.example.telenor.validator;

public class DataValidator {

    public static final String PERSONAL = "personal";
    public static final String BUSINESS = "business";
    public static final String SMALL = "small";
    public static final String BIG = "big";

    public static boolean validateAccount(String account) {
        return account.equals(PERSONAL) || account.equals(BUSINESS);
    }

    public static boolean isNumberPositive(Integer id) {
        return id >= 0;
    }

    public static boolean validateType(String type) {
        return type.equals(SMALL) || type.equals(BIG);
    }

}
