package com.example.telenor.exception;

import com.example.telenor.dto.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(NotAllowableException.class)
    public ResponseEntity<List<ErrorDto>> handleNotAllowableException(NotAllowableException e) {
        final String message = "The value: " + e.getValue() + " , not allowable! " + e.getMessage();
        final List<ErrorDto> errorDto = new ArrayList<>();
        errorDto.add(new ErrorDto("400", message, e.getField()));
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }

}
