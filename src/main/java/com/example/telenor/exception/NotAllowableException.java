package com.example.telenor.exception;

public class NotAllowableException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String field;
    private String value;
    private String message;

    public NotAllowableException(String field, String value, String message) {
        super();
        this.field = field;
        this.value = value;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }
}
