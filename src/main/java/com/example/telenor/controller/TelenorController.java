package com.example.telenor.controller;

import com.example.telenor.service.TelenorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO Note
 * The best practice for naming path is plural when we write root (greeting) path we should write with plural, but I wrote API using specification path
 */
@RestController
@RequestMapping("greeting")
public class TelenorController {

    @Autowired
    TelenorService telenorService;

    @GetMapping
    public String getValue(@RequestParam("id") Integer id, @RequestParam("account") String account, @RequestParam("type") String type) {
        return telenorService.handle(id, account, type);
    }

}