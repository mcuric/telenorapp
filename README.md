# Telenor application

Requirements
------------
Development technical requirements:
- AdoptOpenJDK 11
- Docker


# Step 1 Create build 
`./mvnw install`

**For linux OS**
 `chmod +x mvnw`

# Step 2 Create docker image
`docker build -f Dockerfile .`

# Step 3 Docker compose up
`docker-compose up`


# Test URL
**Task  1**
`localhost:5000/greeting?id=123&type=big&account=personal`

**Task  2**
`localhost:5000/greeting?id=123&type=small&account=business`

**Task  3**
`localhost:5000/greeting?id=123&type=big&account=business`

**Description specification**
 
Welcome to Telenor's take-home assignment
**Congratulations on making it this far! Great job!**
All the necessary information in contained in this text.
Do the absolute bare minimum required for the application. Out of the box configurations will do just fine.
Take as much time as you need for this assignment, but it shouldn’t take more than 60-90min.

In a basic Dockerized Springboot Maven application, develop a single REST endpoint GET /greeting which behaves in the following manner:

1. Given the following input values account=personal and id=123
   and the allowable values for account are personal and business
   and the allowable values for id are all positive integers
   then return "Hi, userId 123".

2. Given the following input values account=business and type=small and
   and the allowable values for account are personal and business
   and the allowable values for type are small and big
   then return an error that the path is not yet implemented.

3. Given the following input values account=business and type=big and
   and the allowable values for account are personal and business
   and the allowable values for type are small and big
   then return "Welcome, business user!".

We should be able to:
- build application with Maven
- build the Docker image and run it
- make a request to localhost:5000/greeting and verify the behavior
  Please provide an archive with the source code and a list of the terminal commands to build and run the application.
  `
